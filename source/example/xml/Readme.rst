XML Examples
============

This folder contains two examples of DDI-CDI in XML form.

-  Process_Example_CDI.xml - This file describes an integration process as a
   set of activities and steps.

-  SPSS_Example_CDI.xml - This file is an XML description which corresponds
   to the SPSS file SPSS_Example.sav.
